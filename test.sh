#!/bin/bash

gcc -O3 -L. -lm -lnnc test/test.c -o nnc_test

export LD_LIBRARY_PATH=.:$LD_LIBRARY_PATH
./nnc_test
