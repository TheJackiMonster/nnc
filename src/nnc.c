/*
This is a free library in C for neuronal networks.
Copyright (C) 2017--2023  TheJackiMonster  thejackimonster@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
		the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "nnc.h"

nnc_val nnc_funcSum(nnc_num n, nnc_val* X) {
	nnc_val result = 0.0;

	for (nnc_num i = 0; i < n; i++) {
		result += X[i];
	}

	return result;
}

nnc_val nnc_funcProduct(nnc_num n, nnc_val* X) {
	nnc_val result = 1.0;

	for (nnc_num i = 0; i < n; i++) {
		result *= X[i];
	}

	return result;
}

nnc_val nnc_funcMin(nnc_num n, nnc_val* X) {
	nnc_val result = INFINITY;

	for (nnc_num i = 0; i < n; i++) {
		if (X[i] < result) {
			result = X[i];
		}
	}

	return result;
}

nnc_val nnc_funcMax(nnc_num n, nnc_val* X) {
	nnc_val result = -INFINITY;

	for (nnc_num i = 0; i < n; i++) {
		if (X[i] > result) {
			result = X[i];
		}
	}

	return result;
}

nnc_val nnc_funcIdentity(nnc_val x) {
	return x;
}

nnc_val nnc_diffIdentity(nnc_val x, nnc_val y) {
	return 1.0;
}

nnc_val nnc_funcSigmoid(nnc_val x) {
	return 1.0 / (1.0 + exp(-x));
}

nnc_val nnc_diffSigmoid(nnc_val x, nnc_val y) {
	return y * (1.0 - y);
}

nnc_val nnc_funcExp(nnc_val x) {
	return exp(x);
}

nnc_val nnc_diffExp(nnc_val x, nnc_val y) {
	return y;
}

nnc_val nnc_funcSqrt(nnc_val x) {
	return sqrt(x);
}

nnc_val nnc_diffSqrt(nnc_val x, nnc_val y) {
	return 0.5 / y;
}

nnc_val nnc_funcTanH(nnc_val x) {
	return tanh(x);
}

nnc_val nnc_diffTanH(nnc_val x, nnc_val y) {
	return 1.0 - y * y;
}

nnc_val nnc_funcSin(nnc_val x) {
	return sin(x);
}

nnc_val nnc_diffSin(nnc_val x, nnc_val y) {
	return cos(x);
}

nnc_val nnc_funcCos(nnc_val x) {
	return cos(x);
}

nnc_val nnc_diffCos(nnc_val x, nnc_val y) {
	return -sin(x);
}

void __nnc_linkset_init(nnc_linkset* set, nnc_num init_size) {
	set->size = init_size;
	set->used = 0;

	set->links = malloc((set->size > 0? set->size : 1) * sizeof(nnc_link*));
}

void __nnc_linkset_dispose(nnc_linkset* set) {
	free(set->links);
}

int __nnc_linkset_update(nnc_linkset* set, nnc_num need) {
	if (need > 0) {
		if (set->size < need) {
			set->size *= 2;
			set->links = realloc(set->links, set->size * sizeof(nnc_link*));

			return 1;
		} else
		if (2 * need <= set->size) {
			set->size /= 2;
			set->links = realloc(set->links, set->size * sizeof(nnc_link*));

			return 1;
		}
	}

	return 0;
}

nnc_num __nnc_linkset_find(nnc_linkset* set, nnc_link* link) {
	nnc_num i = 0;

	while (i < set->used) {
		if (set->links[i] == link) {
			break;
		} else {
			i++;
		}
	}

	return i;
}

nnc_link* nnc_link_add(nnc_node* node, nnc_node* target, nnc_val factor) {
	__nnc_linkset_update(node->output, node->output->used + 1);

	if (__nnc_linkset_update(target->input, target->input->used + 1)) {
		target->cache = realloc(target->cache, (NNC_VALUES_COUNT + target->input->size) * sizeof(nnc_val));
	}

	nnc_link* link = malloc(sizeof(nnc_link));

	link->factor = factor;
	link->in = node;
	link->out = target;

	node->output->links[node->output->used++] = link;
	target->input->links[target->input->used++] = link;

	return link;
}

void nnc_link_remove(nnc_node* node, nnc_link* link) {
	nnc_num id = __nnc_linkset_find(node->output, link);

	if (id < node->output->used) {
		nnc_node* target = link->out;
		nnc_num id_input = __nnc_linkset_find(target->input, link);

		free(link);

		target->input->links[id_input] = target->input->links[--target->input->used];
		node->output->links[id] = node->output->links[--node->output->used];

		if (__nnc_linkset_update(target->input, target->input->used)) {
			target->cache = realloc(target->cache, (NNC_VALUES_COUNT + target->input->size) * sizeof(nnc_val));
		}

		__nnc_linkset_update(node->output, node->output->used);
	}
}

void __nnc_node_init(nnc_node* node, nnc_num in_size, nnc_num out_size, nnc_transmit transmit, nnc_activate activate) {
	node->transmit = transmit;
	node->activate = activate;

	node->input = malloc(sizeof(nnc_linkset));
	node->output = malloc(sizeof(nnc_linkset));

	__nnc_linkset_init(node->input, in_size);
	__nnc_linkset_init(node->output, out_size);

	node->cache = malloc((NNC_VALUES_COUNT + node->input->size) * sizeof(nnc_val));
}

void __nnc_node_dispose(nnc_node* node) {
	while (node->output->used > 0) {
		nnc_link_remove(node, node->output->links[0]);
	}

	free(node->cache);

	__nnc_linkset_dispose(node->output);
	__nnc_linkset_dispose(node->input);

	free(node->output);
	free(node->input);
}

nnc_val nnc_node_get(nnc_node* node) {
	return node->cache[NNC_OUTPUT_OFFSET];
}

void nnc_node_set(nnc_node* node, nnc_val value) {
	node->cache[NNC_INPUT_OFFSET] = value;
}

void nnc_node_expect(nnc_node* node, nnc_val value) {
	node->cache[NNC_HIDDEN_OFFSET] = value;
}

void nnc_node_pull(nnc_node* node) {
	if (node->input->used > 0) {
		for (nnc_num i = 0; i < node->input->used; i++) {
			node->cache[NNC_CACHE_OFFSET + i] = nnc_node_get(node->input->links[i]->in) * node->input->links[i]->factor;
		}

		node->cache[NNC_INPUT_OFFSET] = node->transmit.func(node->input->used, node->cache + NNC_CACHE_OFFSET);
	}
}

void nnc_node_push(nnc_node* node) {
	node->cache[NNC_OUTPUT_OFFSET] = node->activate.func(node->cache[NNC_INPUT_OFFSET]);
}

nnc_group* __nnc_group_create() {
	nnc_group* group = malloc(sizeof(nnc_group));

	group->size = 1;
	group->used = 0;

	group->nodes = malloc(group->size * sizeof(nnc_node*));

	return group;
}

void __nnc_group_destroy(nnc_group* group) {
	for (nnc_num i = 0; i < group->used; i++) {
		__nnc_node_dispose(group->nodes[i]);
	}

	free(group->nodes);
	free(group);
}

nnc_node* __nnc_group_add(nnc_group* group, nnc_node* node) {
	if (group->used + 1 > group->size) {
		group->size *= 2;
		group->nodes = realloc(group->nodes, group->size * sizeof(nnc_node*));
	}

	group->nodes[group->used++] = node;

	return node;
}

nnc_network* nnc_network_create() {
	nnc_network* network = malloc(sizeof(nnc_network));

	network->output = __nnc_group_create();
	network->hidden = __nnc_group_create();
	network->input = __nnc_group_create();

	return network;
}

void nnc_network_destroy(nnc_network* network) {
	__nnc_group_destroy(network->input);
	__nnc_group_destroy(network->hidden);
	__nnc_group_destroy(network->output);

	free(network);
}

nnc_node* nnc_nodeBias(nnc_network* network) {
	nnc_node* node = __nnc_group_add(network->input, malloc(sizeof(nnc_node)));

	__nnc_node_init(node, 0, 1, NNC_Sum, NNC_Identity);

	node->cache[0] = 1.0;
	node->cache[1] = 1.0;

	return node;
}

nnc_node* nnc_nodeInput(nnc_network* network, nnc_activate activate) {
	nnc_node* node = __nnc_group_add(network->input, malloc(sizeof(nnc_node)));

	__nnc_node_init(node, 0, 1, NNC_Sum, activate);

	return node;
}

nnc_node* nnc_nodeOutput(nnc_network* network, nnc_transmit transmit) {
	nnc_node* node = __nnc_group_add(network->output, malloc(sizeof(nnc_node)));

	__nnc_node_init(node, 1, 0, transmit, NNC_Identity);

	return node;
}

nnc_node* nnc_nodeHidden(nnc_network* network, nnc_transmit transmit, nnc_activate activate) {
	nnc_node* node = __nnc_group_add(network->hidden, malloc(sizeof(nnc_node)));

	__nnc_node_init(node, 1, 1, transmit, activate);

	return node;
}

int __nnc_setup_order(nnc_node* node, nnc_node* target) {
	if ((node->output->used == 0) || (node->output->links[0]->out == target)) {
		int result = 1;

		for (nnc_num i = 0; i < node->input->used; i++) {
			result += __nnc_setup_order(node->input->links[i]->in, node);
		}

		return result;
	} else {
		return 0;
	}
}

int __nnc_setup_compare(const void* node0, const void* node1) {
	return __nnc_setup_order(*((nnc_node**) node1), 0) - __nnc_setup_order(*((nnc_node**) node0), 0);
}

void nnc_setup(nnc_network* network) {
	qsort(network->output->nodes, network->output->used, sizeof(nnc_node*), __nnc_setup_compare);
}

void __nnc_update_pull(nnc_node* node, nnc_node* target) {
	if ((node->output->used == 0) || (node->output->links[0]->out == target)) {
		if (node->input->used > 0) {
			for (nnc_num i = 0; i < node->input->used; i++) {
				__nnc_update_pull(node->input->links[i]->in, node);
			}

			nnc_node_pull(node);
		}

		nnc_node_push(node);
	}
}

void nnc_update(nnc_network* network) {
	for (nnc_num i = 0; i < network->output->used; i++) {
		__nnc_update_pull(network->output->nodes[i], 0);
	}
}

void __nnc_upgrade_delta(nnc_node* node, nnc_val epsilon, nnc_val* delta) {
	nnc_val e;

	if (node->output->used > 0) {
		nnc_val d;

		e = 0.0;

		for (nnc_num i = 0; i < node->output->used; i++) {
			__nnc_upgrade_delta(node->output->links[i]->out, epsilon, &d);

			e += (d * node->output->links[i]->factor);

			node->output->links[i]->factor += epsilon * d * node->cache[NNC_OUTPUT_OFFSET];
		}
	} else {
		e = (node->cache[NNC_HIDDEN_OFFSET] - node->cache[NNC_OUTPUT_OFFSET]);
	}

	if (delta != 0) {
		*delta = e * node->activate.diff(node->cache[NNC_INPUT_OFFSET], node->cache[NNC_OUTPUT_OFFSET]);
	}
}

void nnc_upgrade(nnc_network* network, nnc_val epsilon) {
	for (nnc_num i = 0; i < network->input->used; i++) {
		__nnc_upgrade_delta(network->input->nodes[i], epsilon, 0);
	}
}


